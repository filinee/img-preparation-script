#!/bin/bash

#
# Файл-скрипт, склеивает 3 картинки
# также пытается обрезать невидимые слои вокруг изображения
# автор AndreySubbotin andreyit@gmail.com


function clear_combine_png {

path=$1

if [[ -f ${path}/montage.png  ]]
then
    rm ${path}/montage.png
fi

for file in $1/*png
do
    dir_=`dirname ${file}`
    filename=`basename ${file}`

    echo "File was:" `file ${file}`
    new_file_name=${dir_}/c_${filename%.*}.${filename##*.}
    convert ${file} -resize x508 ${new_file_name} 
    #convert ${file} -fuzz 40% -trim +repage ${new_file_name}
    echo "File now:" `file ${new_file_name}`
    #"${dir_}/c_${filename%.*}.${filename##*.}"

done    
montage ${path}/c_?.png ${path}/c_??.png -tile x1 -geometry +0+0 -background none ${path}/montage.png
rm ${path}/c_*.png
}


function combine_stripe {
    files_list=$@
    echo ${files_list}

    montage ${files_list} -tile 1x -geometry +0+0 -background none montage.png
}


montage_files=""
for dir in *
do
    if [[ -d ${dir}  ]]
    then
        echo ${dir}
        clear_combine_png ${dir}
        montage_files="${montage_files} ${dir}/montage.png"
    fi
done

combine_stripe ${montage_files}

mv montage.png spritesheet_girl.png
pngquant --speed 1 --force spritesheet_girl.png
